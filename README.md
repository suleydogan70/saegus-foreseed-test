# Saegus / Foreseed

Application de gestion de tâches, similaire à Google Tasks.

## Pré-requis

- docker `18.03.0-ce`
- docker-compose `1.20.1`

## Installation

Tout d'abord, clonez le projet.

```
git clone https://gitlab.com/suleydogan70/saegus-foreseed-test.git
```

Placez-vous dans le projet, puis lancez le docker compose.

```
cd saegus-foreseed-test
docker-compose up
```

##Les ports utilisés

Docker sur Windows se positionne sur l'IP `192.168.99.100`
- NodeJS API `4000:4000`
- ReactJS front_end `8080:8080`
- MongoDB `27017:27017`

## Fonctionnalités

- Créer un utilisateur
- Connecter un utilisateur
- Ajouter une liste de tâches
- Ajouter une tâche dans une liste
- Modifier la tâche
