const JWT = require('jsonwebtoken')
const ObjectId = require('mongodb').ObjectID;

class Create {
  constructor(app, conn) {
    this.app = app
    this.conn = conn

    this.run()
  }

  middleware() {
    this.app.delete('/taskList/:id', this.endPointCb.bind(this))
  }

  async endPointCb(req, res) {
    const { id } = req.params
    try {
      await this.conn.collection('tasks').deleteMany({ id_list: id })
      await this.conn.collection('taskList').deleteOne({ _id: new ObjectId(id) })
      res.json({ success: true })
    } catch (e) {
      console.log(e)
      res.json({ success: false })
    }

  }

  run() {
    this.middleware()
  }
}

module.exports = Create
