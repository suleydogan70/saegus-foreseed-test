const JWT = require('jsonwebtoken')
const ObjectId = require('mongodb').ObjectID;

class Create {
  constructor(app, conn) {
    this.app = app
    this.conn = conn

    this.run()
  }

  verifyToken(req, res, next) {
    const { authorization } = req.headers
    if (authorization === undefined) {
      // res.status(403)
    } else {
      const bearer = authorization.split(' ')
      req.token = bearer[1]
    }
    next()
  }

  middleware() {
    this.app.post('/taskList/create', this.verifyToken, this.endPointCb.bind(this))
  }

  async endPointCb(req, res) {
    const { taskListName } = req.body
    try {
      JWT.verify(req.token, 'SPLITZIZSECRETKEY', async (err, data) => {
        const existing = await this.conn.collection('taskList').findOne({ taskListName, id_user: data.sub._id })
        if (!existing) {
          const insertion = await this.conn.collection('taskList').insertOne({ taskListName, id_user: data.sub._id })
          const newItem = await this.conn.collection('taskList').findOne({ _id: new ObjectId(insertion.insertedId) })
          res.json({ success: true, taskList: newItem })
        } else {
          res.json({ error: 'task already existing' })
        }
      })
    } catch (e) {
      console.log(e)
      res.json({ success: false })
    }

  }

  run() {
    this.middleware()
  }
}

module.exports = Create
