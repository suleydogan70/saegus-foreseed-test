const JWT = require('jsonwebtoken')

class Tasks {
  constructor(app, conn) {
    this.app = app
    this.conn = conn

    this.run()
  }

  verifyToken(req, res, next) {
    const { authorization } = req.headers
    if (authorization === undefined) {
      // res.status(403)
    } else {
      const bearer = authorization.split(' ')
      req.token = bearer[1]
    }
    next()
  }

  middleware() {
    this.app.get('/taskList/:id/tasks', this.verifyToken, this.endPointCb.bind(this))
  }

  async endPointCb(req, res) {
    try {
      JWT.verify(req.token, 'SPLITZIZSECRETKEY', async (err, data) => {
        if (err) throw err
        const tasks = (await this.conn.collection('tasks').find({ id_list: req.params.id }).toArray())
        res.json(tasks)
      })
    } catch (e) {
      console.log(e)
      res.json({ error: 'une erreur est survenue' })
    }

  }

  run() {
    this.middleware()
  }
}

module.exports = Tasks
