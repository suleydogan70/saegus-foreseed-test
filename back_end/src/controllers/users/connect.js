const JWT = require('jsonwebtoken')
const ObjectId = require('mongodb').ObjectID;

class Connect {
  constructor(app, conn) {
    this.app = app
    this.conn = conn

    this.run()
  }

  signToken (user) {
    return JWT.sign({
      iss: 'Splitziz',
      sub: user
    }, 'SPLITZIZSECRETKEY')
  }

  verifyToken(req, res, next) {
    const { authorization } = req.headers
    if (authorization === undefined) {
      // res.status(403)
    } else {
      const bearer = authorization.split(' ')
      req.token = bearer[1]
    }
    next()
  }

  middleware() {
    this.app.post('/users/connect', this.verifyToken, this.endPointCb.bind(this))
  }

  async endPointCb(req, res) {
    const { mail, password } = req.body
    try {
      let query = {}
      if (mail && password) {
        query = {
          mail,
          password
        }
      } else {
        await new Promise(resolve => {
          JWT.verify(req.token, 'SPLITZIZSECRETKEY', (err, data) => {
            query = { _id: new ObjectId(data.sub._id) }
            resolve()
          })
        })
      }
      const user = await this.conn.collection('user').findOne(query, {
        projection: {
          lastname: 1,
          firstname: 1,
          mail: 1
        }
      })
      if (!user) {
        res.json({ error: 'User not existing' })
      } else {
        res.json({ user, token: this.signToken(user) })
      }
    } catch (e) {
      console.log(e)
      res.json({ error: 'une erreur est survenue' })
    }

  }

  run() {
    this.middleware()
  }
}

module.exports = Connect
