const JWT = require('jsonwebtoken')

class Connect {
  constructor(app, conn) {
    this.app = app
    this.conn = conn

    this.run()
  }

  verifyToken(req, res, next) {
    const { authorization } = req.headers
    if (authorization === undefined) {
      // res.status(403)
    } else {
      const bearer = authorization.split(' ')
      req.token = bearer[1]
    }
    next()
  }

  middleware() {
    this.app.get('/users/taskList', this.verifyToken, this.endPointCb.bind(this))
  }

  async endPointCb(req, res) {
    try {
      JWT.verify(req.token, 'SPLITZIZSECRETKEY', async (err, data) => {
        const taskList = (await this.conn.collection('taskList').find({ id_user: data.sub._id }).toArray())
        res.json(taskList)
      })
    } catch (e) {
      console.log(e)
      res.json({ error: 'une erreur est survenue' })
    }

  }

  run() {
    this.middleware()
  }
}

module.exports = Connect
