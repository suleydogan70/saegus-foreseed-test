class Create {
  constructor(app, conn) {
    this.app = app
    this.conn = conn

    this.run()
  }

  middleware() {
    this.app.post('/users/create', this.endPointCb.bind(this))
  }

  async endPointCb(req, res) {
    const { lastname, firstname, mail, password } = req.body
    try {
      await this.conn.collection('user').insertOne({
        lastname, firstname, mail, password
      })
      res.json({ success: true })
    } catch (e) {
      console.log(e)
      res.json({ success: false })
    }

  }

  run() {
    this.middleware()
  }
}

module.exports = Create
