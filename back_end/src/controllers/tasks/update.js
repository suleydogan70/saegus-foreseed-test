const ObjectId = require('mongodb').ObjectID;

class Update {
  constructor(app, conn) {
    this.app = app
    this.conn = conn

    this.run()
  }

  middleware() {
    this.app.put('/tasks/:id', this.endPointCb.bind(this))
  }

  async endPointCb(req, res) {
    const { id } = req.params
    try {
      const task = await this.conn.collection('tasks').updateOne({
        _id: new ObjectId(id)
      }, {
        $set: {
          [req.body.field]: req.body.value
        }
      })
      res.json({ success: true, task })
    } catch (e) {
      console.log(e)
      res.json({ success: false })
    }
  }

  run() {
    this.middleware()
  }
}

module.exports = Update
