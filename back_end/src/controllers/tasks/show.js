const ObjectId = require('mongodb').ObjectID;

class Show {
  constructor(app, conn) {
    this.app = app
    this.conn = conn

    this.run()
  }

  middleware() {
    this.app.get('/tasks/:id', this.endPointCb.bind(this))
  }

  async endPointCb(req, res) {
    const { id } = req.params
    try {
      const task = await this.conn.collection('tasks').findOne({ _id: new ObjectId(id) })
      res.json({ success: true, task })
    } catch (e) {
      console.log(e)
      res.json({ success: false })
    }

  }

  run() {
    this.middleware()
  }
}

module.exports = Show
