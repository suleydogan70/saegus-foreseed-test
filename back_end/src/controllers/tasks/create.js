const ObjectId = require('mongodb').ObjectID;

class Create {
  constructor(app, conn) {
    this.app = app
    this.conn = conn

    this.run()
  }

  middleware() {
    this.app.post('/tasks/create', this.endPointCb.bind(this))
  }

  async endPointCb(req, res) {
    const { short, long, date, id_list } = req.body
    try {
      const insertion = await this.conn.collection('tasks').insertOne({
        short, long, date, id_list, state: 'WIP'
      })
      const task = await this.conn.collection('tasks').findOne({ _id: new ObjectId(insertion.insertedId) })
      res.json({ success: true, task })
    } catch (e) {
      console.log(e)
      res.json({ success: false })
    }

  }

  run() {
    this.middleware()
  }
}

module.exports = Create
