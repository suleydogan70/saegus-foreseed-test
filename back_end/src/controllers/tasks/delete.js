const ObjectId = require('mongodb').ObjectID;

class Delete {
  constructor(app, conn) {
    this.app = app
    this.conn = conn

    this.run()
  }

  middleware() {
    this.app.delete('/tasks/:id', this.endPointCb.bind(this))
  }

  async endPointCb(req, res) {
    const { id } = req.params
    try {
      const task = await this.conn.collection('tasks').deleteOne({
        _id: new ObjectId(id)
      })
      res.json({ success: true })
    } catch (e) {
      console.log(e)
      res.json({ success: false })
    }
  }

  run() {
    this.middleware()
  }
}

module.exports = Delete
