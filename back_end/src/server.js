const { MongoClient } = require('mongodb')
const compression = require('compression')
const bodyParser = require('body-parser')
const express = require('express')
const helmet = require('helmet')
const cors = require('cors')

const routes = require('./routes.js')
const PORT = 4000

class TestSaegusBackEnd {
  constructor () {
    this.app = express()
    this.conn = null

    this.run()
  }

  middleware () {
    this.app.use(compression())
    this.app.use(cors())
    this.app.use(bodyParser.urlencoded({ 'extended': true }))
    this.app.use(bodyParser.json())
  }

  routes () {
    new routes.users.CreateUser(this.app, this.conn)
    new routes.users.ConnectUser(this.app, this.conn)
    new routes.users.UserTaskList(this.app, this.conn)

    new routes.taskList.CreateTaskList(this.app, this.conn)
    new routes.taskList.GetListTasks(this.app, this.conn)
    new routes.taskList.DeleteTaskList(this.app, this.conn)

    new routes.tasks.CreateTask(this.app, this.conn)
    new routes.tasks.ShowTask(this.app, this.conn)
    new routes.tasks.UpdateTask(this.app, this.conn)
    new routes.tasks.DeleteTask(this.app, this.conn)

    this.app.use((req, res) => {
      res.status(404).json({
        'code': 404,
        'message': 'Not Found'
      })
    })
  }

  security () {
    this.app.use(helmet())
    this.app.disable('x-powered-by')
  }

  async run () {
    try {
      this.security()
      this.middleware()
      await new Promise (resolve => {
        MongoClient.connect('mongodb://192.168.99.100:27017', {
          useNewUrlParser: true,
          useUnifiedTopology: true
        }, (err, client) => {
          if (err) {
            console.log(err)
          } else {
            console.log('connected to db')
            this.conn = client.db('test_saegus')
            resolve()
          }
        })
      })
      this.routes()
      this.app.listen(PORT, () => console.log(`Listening on PORT:${PORT}`))
    } catch (e) {
      console.error(`[ERROR] Server -> ${e}`)
    }
  }
}

module.exports = TestSaegusBackEnd
