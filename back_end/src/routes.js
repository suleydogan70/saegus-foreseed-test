const CreateUser = require('./controllers/users/create.js')
const ConnectUser = require('./controllers/users/connect.js')
const UserTaskList = require('./controllers/users/taskList.js')

const CreateTaskList = require('./controllers/taskList/create.js')
const GetListTasks = require('./controllers/taskList/tasks.js')
const DeleteTaskList = require('./controllers/taskList/delete.js')

const CreateTask = require('./controllers/tasks/create.js')
const ShowTask = require('./controllers/tasks/show.js')
const UpdateTask = require('./controllers/tasks/update.js')
const DeleteTask = require('./controllers/tasks/delete.js')

module.exports = {
  users: {
    CreateUser,
    ConnectUser,
    UserTaskList
  },
  taskList: {
    CreateTaskList,
    GetListTasks,
    DeleteTaskList
  },
  tasks: {
    CreateTask,
    ShowTask,
    UpdateTask,
    DeleteTask
  }
}
