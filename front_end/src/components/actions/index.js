import axios from 'axios'

export const createUser = payload => axios.post('/users/create', payload).then(response => response.data)

export const connectUser = payload => axios.post('/users/connect', payload).then(response => response.data)

export const createTaskList = payload => axios.post('/taskList/create', payload).then(response => response.data)

export const getUserTaskList = () => axios.get('/users/taskList').then(response => response.data)

export const deleteTaskList = id => axios.delete(`/taskList/${id}`).then(response => response.data)

export const getListTasks = id => axios.get(`/taskList/${id}/tasks`).then(response => response.data)

export const addTaskToList = payload => axios.post('/tasks/create', payload).then(response => response.data)

export const getTaskInformations = id => axios.get(`/tasks/${id}`).then(response => response.data.task)

export const updateTaskInformations = (id, field, value) => axios.put(`/tasks/${id}`, { field, value }).then(response => response.data)

export const deleteTaskAPI = id => axios.delete(`/tasks/${id}`).then(response => response.data)
