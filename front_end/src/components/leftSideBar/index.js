import React, { Component } from 'react'
import { connect } from 'react-redux'
import { createTaskList, getUserTaskList, deleteTaskList } from '../actions'
import actionsType from '../actions/types'

class LeftSideBar extends Component {
  constructor(props) {
    super(props)

    this.state = {
      taskListName: '',
      taskListList: [],
      error: null,
      askDelete: false,
      chosenList: null
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.setCurrentList = this.setCurrentList.bind(this)
  }

  componentDidMount() {
    const { dispatch } = this.props
    getUserTaskList().then(taskListList => dispatch({
      type: actionsType.GET_TASKLISTS,
      payload: taskListList
    }))
  }

  setCurrentList(taskListItem) {
    const { updateCurrentList, updateCurrentTask } = this.props
    updateCurrentList(taskListItem)
    updateCurrentTask(null)
  }

  DeleteTaskList(id, force = false) {
    const { askDelete } = this.state
    const { updateCurrentList, dispatch, updateCurrentTask } = this.props
    if (askDelete && force) {
      deleteTaskList(id).then((response) => {
        if (response.success) {
          this.setState({
            askDelete: false,
            chosenList: null
          })
          dispatch({
            type: actionsType.DELETE_LIST_ITEM,
            payload: id
          })
          updateCurrentList({ _id: null })
          updateCurrentTask(null)
        }
      })
    } else {
      this.setState({ askDelete: true, chosenList: id })
    }
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  handleSubmit(e) {
    const { taskListName } = this.state
    const { dispatch } = this.props
    e.preventDefault()
    createTaskList({ taskListName }).then((response) => {
      if (response.error) {
        this.setState({ error: response.error })
      } else {
        dispatch({
          type: actionsType.ADD_TASK_LIST,
          payload: response.taskList
        })
        this.setState({ error: null, taskListName: '' })
      }
    })
  }

  render() {
    const {
      taskListName,
      error,
      askDelete,
      chosenList
    } = this.state
    const { currentList, taskList } = this.props
    return (
      <nav className="nav leftSideBar">
        <h2>Liste des tâches</h2>
        { error ? <p className="alert alert__danger">{error}</p> : '' }
        <form className="form" onSubmit={this.handleSubmit}>
          <input placeholder="Ajouter une tâche" name="taskListName" className="input" type="text" value={taskListName} onChange={this.handleChange} />
          <button className="d-n" type="submit">+</button>
        </form>
        <ul>
          {
            taskList.map(taskListItem => (
              <li className={`nav-item ${currentList !== null && currentList._id === taskListItem._id ? 'active' : ''}`} key={taskListItem._id} onClick={() => this.setCurrentList(taskListItem)}>
                {taskListItem.taskListName}
                <svg className="pointer" onClick={() => this.DeleteTaskList(taskListItem._id)} width="13" height="15" viewBox="0 0 13 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M8.37657 5.43457C8.17748 5.43457 8.01613 5.59182 8.01613 5.78585V12.4252C8.01613 12.6191 8.17748 12.7765 8.37657 12.7765C8.57565 12.7765 8.737 12.6191 8.737 12.4252V5.78585C8.737 5.59182 8.57565 5.43457 8.37657 5.43457Z" fill="#CC0000" />
                  <path d="M4.12344 5.43457C3.92435 5.43457 3.763 5.59182 3.763 5.78585V12.4252C3.763 12.6191 3.92435 12.7765 4.12344 12.7765C4.32252 12.7765 4.48387 12.6191 4.48387 12.4252V5.78585C4.48387 5.59182 4.32252 5.43457 4.12344 5.43457Z" fill="#CC0000" />
                  <path d="M1.02369 4.46564V13.1206C1.02369 13.6322 1.21616 14.1126 1.55238 14.4573C1.88705 14.8029 2.3528 14.9992 2.84023 15H9.65977C10.1473 14.9992 10.6131 14.8029 10.9476 14.4573C11.2838 14.1126 11.4763 13.6322 11.4763 13.1206V4.46564C12.1447 4.29275 12.5777 3.66345 12.4883 2.99504C12.3988 2.32677 11.8146 1.82688 11.1231 1.82674H9.27765V1.38763C9.27977 1.01837 9.12996 0.663786 8.86175 0.402928C8.59353 0.142208 8.22915 -0.00297272 7.85028 4.61562e-05H4.64973C4.27085 -0.00297272 3.90647 0.142208 3.63826 0.402928C3.37004 0.663786 3.22024 1.01837 3.22235 1.38763V1.82674H1.37695C0.685364 1.82688 0.101206 2.32677 0.0116603 2.99504C-0.0777445 3.66345 0.355341 4.29275 1.02369 4.46564ZM9.65977 14.2974H2.84023C2.22397 14.2974 1.74456 13.7815 1.74456 13.1206V4.49652H10.7554V13.1206C10.7554 13.7815 10.276 14.2974 9.65977 14.2974ZM3.94322 1.38763C3.94082 1.20471 4.0146 1.02866 4.14779 0.899533C4.28084 0.770407 4.46191 0.699464 4.64973 0.70262H7.85028C8.0381 0.699464 8.21916 0.770407 8.35221 0.899533C8.4854 1.02852 8.55918 1.20471 8.55678 1.38763V1.82674H3.94322V1.38763ZM1.37695 2.52931H11.1231C11.4814 2.52931 11.7718 2.8124 11.7718 3.16163C11.7718 3.51086 11.4814 3.79395 11.1231 3.79395H1.37695C1.01863 3.79395 0.728166 3.51086 0.728166 3.16163C0.728166 2.8124 1.01863 2.52931 1.37695 2.52931Z" fill="#CC0000" />
                  <path d="M6.25 5.43457C6.05092 5.43457 5.88957 5.59182 5.88957 5.78585V12.4252C5.88957 12.6191 6.05092 12.7765 6.25 12.7765C6.44909 12.7765 6.61044 12.6191 6.61044 12.4252V5.78585C6.61044 5.59182 6.44909 5.43457 6.25 5.43457Z" fill="#CC0000" />
                </svg>
              </li>
            ))
          }
        </ul>
        {
          askDelete
            ? (
              <div className="modal">
                <div className="modal-inside">
                  <p>
                    {
                      'Êtes-vous sûr de vouloir supprimer cette liste ? '
                    + 'Les sous tâches de celle-ci risquent d\'être perdues'
                    }
                  </p>
                  <div className="button-group">
                    <button type="button" className="button button__primary" onClick={() => this.setState({ askDelete: false })}>Annuler</button>
                    <button type="button" className="button button__tertiary" onClick={() => this.DeleteTaskList(chosenList, true)}>Supprimer</button>
                  </div>
                </div>
              </div>
            )
            : ''
        }
      </nav>
    )
  }
}

export default connect(state => state)(LeftSideBar)
