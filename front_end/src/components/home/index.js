import React, { Component } from 'react'
import LeftSideBar from '../leftSideBar'
import Main from '../main'
import RightSideBar from '../rightSideBar'

class Home extends Component {
  constructor(props) {
    super(props)

    this.state = {
      currentList: {
        _id: null
      },
      currentTask: null
    }

    this.updateCurrentList = this.updateCurrentList.bind(this)
    this.updateCurrentTask = this.updateCurrentTask.bind(this)
  }

  updateCurrentList(currentList) {
    this.setState({ currentList })
  }

  updateCurrentTask(currentTask) {
    this.setState({ currentTask })
  }

  render() {
    const { currentList, currentTask } = this.state
    return (
      <div className="main-content">
        <LeftSideBar
          currentList={currentList}
          updateCurrentList={this.updateCurrentList}
          updateCurrentTask={this.updateCurrentTask}
        />
        <Main
          currentList={currentList}
          currentTask={currentTask}
          updateCurrentTask={this.updateCurrentTask}
        />
        <RightSideBar currentTask={currentTask} />
      </div>
    )
  }
}

export default Home
