import React, { Component } from 'react'
import { connect } from 'react-redux'
import { updateTaskInformations } from '../../actions'
import actionsType from '../../actions/types'

class Card extends Component {
  constructor(props) {
    super(props)

    this.changeTaskState = this.changeTaskState.bind(this)
  }

  changeTaskState(id, state) {
    const { dispatch, task } = this.props
    if (state === 'WIP') {
      updateTaskInformations(id, 'state', 'done').then(() => {
        dispatch({
          type: actionsType.UPDATE_TASK,
          payload: { ...task, state: 'done' }
        })
      })
    } else {
      updateTaskInformations(id, 'state', 'WIP').then(() => {
        dispatch({
          type: actionsType.UPDATE_TASK,
          payload: { ...task, state: 'WIP' }
        })
      })
    }
  }

  render() {
    const {
      task,
      updateCurrentTask,
      currentTask
    } = this.props
    return (
      <div
        key={task._id}
        className={`card card__dark card__action spaced ${currentTask === task._id ? 'active' : ''}`}
        onClick={() => updateCurrentTask(task._id)}
      >
        <h3>{task.short}</h3>
        <span className={`bullet bullet__${task.state}`} onClick={() => this.changeTaskState(task._id, task.state)} />
      </div>
    )
  }
}

export default connect()(Card)
