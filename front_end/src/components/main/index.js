import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addTaskToList, getListTasks } from '../actions'
import actionsType from '../actions/types'
import Card from './components/card'

class Main extends Component {
  constructor(props) {
    super(props)

    this.state = {
      short: '',
      long: '',
      date: '',
      tasks: []
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidUpdate(prevProps) {
    const { currentList, dispatch } = this.props
    if (prevProps.currentList._id !== currentList._id) {
      getListTasks(currentList._id).then(tasks => dispatch({
        type: actionsType.GET_TASKS,
        payload: tasks
      }))
    }
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  handleSubmit(e) {
    const { currentList, dispatch } = this.props
    e.preventDefault()
    addTaskToList({ ...this.state, id_list: currentList._id }).then((response) => {
      console.log(response)
      if (response.success) {
        dispatch({
          type: actionsType.ADD_TASK,
          payload: response.task
        })
        this.setState({
          short: '',
          long: '',
          date: ''
        })
      }
    })
  }

  render() {
    const {
      short,
      long,
      date
    } = this.state
    const {
      currentList,
      currentTask,
      updateCurrentTask,
      tasks
    } = this.props
    return (
      <div className="spaced">
        {
          currentList._id === null
            ? <p>Sélectionnez une liste de tâche</p>
            : (
              <div>
                <h2>{currentList.taskListName}</h2>
                <div className="grid grid__3">
                  {
                    tasks.map(task => (
                      <Card
                        key={task._id}
                        task={task}
                        updateCurrentTask={updateCurrentTask}
                        currentTask={currentTask}
                      />
                    ))
                  }
                  <form className="form form__dark spaced addTask card" onSubmit={this.handleSubmit}>
                    <h3>Ajouter une tâche</h3>
                    <input name="short" type="text" className="input input__dark" placeholder="Short Description" required value={short} onChange={this.handleChange} />
                    <textarea name="long" className="input input__dark" placeholder="Long Description" value={long} onChange={this.handleChange} />
                    <input name="date" type="date" required className="input input__dark" value={date} onChange={this.handleChange} />
                    <button type="submit" className="button button__primary">Ajouter</button>
                  </form>
                </div>
              </div>
            )
        }
      </div>
    )
  }
}

export default connect(state => state)(Main)
