import React, { Component } from 'react'
import { connect } from 'react-redux'
import axios from 'axios'
import { connectUser } from '../../actions'
import actionsType from '../../actions/types'

class Connexion extends Component {
  constructor(props) {
    super(props)

    this.state = {
      mail: '',
      password: '',
      error: null
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  handleSubmit(e) {
    const { dispatch } = this.props
    e.preventDefault()
    connectUser(this.state).then((payload) => {
      if (payload.error) {
        this.setState({ error: payload.error })
      } else {
        localStorage.setItem('authToken', payload.token)
        axios.defaults.headers.common = { Authorization: `Bearer ${payload.token}` }
        dispatch({
          type: actionsType.CONNECT_USER,
          payload
        })
      }
    })
  }

  render() {
    const { mail, password, error } = this.state
    return (
      <form className="form" onSubmit={this.handleSubmit}>
        <div>
          <h2>Connexion</h2>
          <p className="center">
            Connectez-vous à votre espace personnel.
          </p>
          { error ? <p className="alert alert__danger">{error}</p> : '' }
          <input required name="mail" type="email" placeholder="Email" className="input" onChange={this.handleChange} value={mail} />
          <input required name="password" type="password" placeholder="Password" className="input" onChange={this.handleChange} value={password} />
          <button type="submit" className="button button__primary">Connexion</button>
        </div>
      </form>
    )
  }
}

export default connect()(Connexion)
