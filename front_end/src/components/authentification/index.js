import React, { Component } from 'react'
import Connexion from './connexion'
import Register from './register'

class Authentification extends Component {
  render() {
    return (
      <div className="authentification-page">
        <Connexion />
        <Register />
      </div>
    )
  }
}

export default Authentification
