import React, { Component } from 'react'
import { createUser } from '../../actions'

class Register extends Component {
  constructor(props) {
    super(props)

    this.state = {
      lastname: '',
      firstname: '',
      mail: '',
      mail2: '',
      password: '',
      password2: '',
      error: null,
      success: false
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  handleSubmit(e) {
    const {
      mail,
      mail2,
      password,
      password2
    } = this.state
    e.preventDefault()
    if (mail !== mail2) {
      this.setState({ error: 'Mail confirmation is wrong' })
    } else if (password !== password2) {
      this.setState({ error: 'Password confirmation is wrong' })
    } else {
      this.setState({ error: null })
    }
    createUser(this.state).then(user => this.setState({
      success: user.success,
      lastname: '',
      firstname: '',
      mail: '',
      mail2: '',
      password: '',
      password2: ''
    }))
  }

  render() {
    const {
      lastname,
      firstname,
      mail,
      mail2,
      password,
      password2,
      error,
      success
    } = this.state
    return (
      <form className="form form__dark" onSubmit={this.handleSubmit}>
        <div>
          <h2>Register</h2>
          <p className="center">
            Créez-vous un compte.
          </p>
          { error ? <p className="alert alert__danger center">{error}</p> : '' }
          {
            success
              ? (
                <div>
                  <p className="alert alert__success center">Votre compte a bien été créé</p>
                  <button type="submit" className="button button__primary" onClick={() => this.setState({ success: false })}>Créer un autre compte</button>
                </div>
              )
              : (
                <div>
                  <input required name="lastname" type="text" placeholder="Lastname" className="input input__dark" value={lastname} onChange={this.handleChange} />
                  <input required name="firstname" type="text" placeholder="Firstname" className="input input__dark" value={firstname} onChange={this.handleChange} />
                  <input required name="mail" type="email" placeholder="Email" className="input input__dark" value={mail} onChange={this.handleChange} />
                  <input required name="mail2" type="email" placeholder="Confirm Email" className="input input__dark" value={mail2} onChange={this.handleChange} />
                  <input required name="password" type="password" placeholder="Password" className="input input__dark" id="register_password" value={password} onChange={this.handleChange} />
                  <input required name="password2" type="password" placeholder="Confirm Password" className="input input__dark" id="register_password" value={password2} onChange={this.handleChange} />
                  <button type="submit" className="button button__primary">Register</button>
                </div>
              )
          }
        </div>
      </form>
    )
  }
}

export default Register
