import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getTaskInformations, updateTaskInformations, deleteTaskAPI } from '../actions'
import actionsType from '../actions/types'

class RightSideBar extends Component {
  constructor(props) {
    super(props)

    this.state = {
      short: '',
      long: '',
      date: '',
      _id: ''
    }

    this.handleChange = this.handleChange.bind(this)
    this.deleteTask = this.deleteTask.bind(this)
    this.emptyState = this.emptyState.bind(this)
  }

  componentDidUpdate(prevProps) {
    const { currentTask } = this.props
    if (prevProps.currentTask !== currentTask) {
      getTaskInformations(currentTask).then(task => this.setState({
        short: task.short,
        long: task.long,
        date: task.date,
        _id: task._id
      }))
    }
  }

  emptyState() {
    this.setState({
      short: '',
      long: '',
      date: '',
      _id: ''
    })
  }

  handleChange(e) {
    const { _id } = this.state
    const { dispatch } = this.props
    this.setState({ [e.target.name]: e.target.value })
    updateTaskInformations(_id, e.target.name, e.target.value).then(() => {
      dispatch({
        type: actionsType.UPDATE_TASK,
        payload: this.state
      })
    })
  }

  deleteTask(id) {
    const { dispatch } = this.props
    deleteTaskAPI(id).then(() => {
      dispatch({
        type: actionsType.DELETE_TASK,
        payload: id
      })
    })
  }

  render() {
    const {
      short,
      long,
      date,
      _id
    } = this.state
    const { currentTask } = this.props
    return (
      <div className="nav rightSideBar">
        <h2>Informations</h2>
        {
          currentTask !== null && short !== ''
            ? (
              <div>
                <div className="form">
                  <input
                    placeholder="Short description"
                    name="short"
                    className="input"
                    type="text"
                    value={short}
                    onChange={this.handleChange}
                  />
                  <textarea
                    placeholder="Long description"
                    name="long"
                    className="input"
                    value={long}
                    onChange={this.handleChange}
                  />
                  <input
                    placeholder="date"
                    name="date"
                    className="input"
                    type="date"
                    value={date}
                    onChange={this.handleChange}
                  />
                </div>
                <button type="button" className="button button__tertiary" onClick={() => this.deleteTask(_id)}>Supprimer</button>
              </div>
            )
            : <p>Aucune tâche n&apos;a été choisie</p>
        }
      </div>
    )
  }
}

export default connect(state => state)(RightSideBar)
