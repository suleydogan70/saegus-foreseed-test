import React, { Component } from 'react'
import { connect } from 'react-redux'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { connectUser } from './components/actions'

import Home from './components/home'

import Authentification from './components/authentification'
import actionsType from './components/actions/types'

class Routes extends Component {
  componentDidMount() {
    const { isAuth, dispatch } = this.props
    const token = localStorage.getItem('authToken')
    if (!isAuth && token !== null) {
      connectUser(this.state).then((payload) => {
        if (payload.error) {
          this.setState({ error: payload.error })
        } else {
          localStorage.setItem('authToken', payload.token)
          dispatch({
            type: actionsType.CONNECT_USER,
            payload
          })
        }
      })
    }
  }

  render() {
    const { isAuth } = this.props
    return (
      <BrowserRouter>
        <main>
          <Switch>
            <Route path="/" component={isAuth ? Home : Authentification} exact />
          </Switch>
        </main>
      </BrowserRouter>
    )
  }
}

const mapStateToProps = state => ({
  isAuth: state.auth.isAuth
})

export default connect(mapStateToProps)(Routes)
