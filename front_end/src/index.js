import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import axios from 'axios'

import store from './store'
import './style.scss'

import Routes from './routes'

axios.defaults.baseURL = 'http://local.docker:4000/'
const token = localStorage.getItem('authToken')
if (token) {
  axios.defaults.headers.common = { Authorization: `Bearer ${token}` }
}

const App = () => (
  <Provider store={store}>
    <Routes />
  </Provider>
)

ReactDOM.render(<App />, document.getElementById('app'))
