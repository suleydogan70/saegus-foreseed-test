import { combineReducers } from 'redux'
import auth from './reducers/auth'
import taskList from './reducers/taskList'
import tasks from './reducers/tasks'

export default combineReducers({
  auth,
  taskList,
  tasks
})
