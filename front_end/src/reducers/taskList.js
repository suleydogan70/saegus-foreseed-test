import { List } from 'immutable'
import actionTypes from '../components/actions/types'

const DEFAULT_STATE = []

const getTaskLists = payload => List(DEFAULT_STATE).concat(payload).toJS()

const addListItem = (state, payload) => state.concat(payload)

const deleteListItem = (state, payload) => (
  state.filter(taskList => taskList._id !== payload)
)

const taskList = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case actionTypes.GET_TASKLISTS:
      return getTaskLists(action.payload)
    case actionTypes.ADD_TASK_LIST:
      return addListItem(state, action.payload)
    case actionTypes.DELETE_LIST_ITEM:
      return deleteListItem(state, action.payload)
    default:
      return state
  }
}

export default taskList
