import { fromJS } from 'immutable'
import actionTypes from '../components/actions/types'

const DEFAULT_STATE = {
  user: null,
  isAuth: false,
  token: null
}

const connectUser = payload => fromJS(DEFAULT_STATE)
  .set('isAuth', true)
  .set('token', payload.token)
  .set('user', payload.user)
  .toJS()

const auth = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case actionTypes.CONNECT_USER:
      return connectUser(action.payload)
    default:
      return state
  }
}

export default auth
