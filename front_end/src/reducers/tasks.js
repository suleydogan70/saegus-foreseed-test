import { List } from 'immutable'
import actionTypes from '../components/actions/types'

const DEFAULT_STATE = []

const getTasks = payload => List(DEFAULT_STATE).concat(payload).toJS()

const updateTask = (state, payload) => state.map((task) => {
  if (task._id === payload._id) {
    return { ...task, ...payload }
  }
  return task
})

const addTask = (state, payload) => state.concat(payload)

const deleteTask = (state, payload) => state.filter(task => task._id !== payload)

const tasks = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case actionTypes.GET_TASKS:
      return getTasks(action.payload)
    case actionTypes.ADD_TASK:
      return addTask(state, action.payload)
    case actionTypes.UPDATE_TASK:
      return updateTask(state, action.payload)
    case actionTypes.DELETE_TASK:
      return deleteTask(state, action.payload)
    default:
      return state
  }
}

export default tasks
